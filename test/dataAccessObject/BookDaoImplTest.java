package dataAccessObject;

import java.util.List;
import model.Book;

/**
 *
 * @author Tiago Martins Vargas
 *
 */
public class BookDaoImplTest {

    private Book book;
    private BookDaoImpl daoImpl;

    public BookDaoImplTest() {
    }

//    @org.junit.Test
    public void testSave() {
        System.out.println("Salvar");
        book = new Book(null, "Harry Potter e a Pedra Filosofal", "Rocco", "J.K. Rowling", "6532-2", "2000", "Harry, filho de...");
        daoImpl = new BookDaoImpl();
        daoImpl.save(book);
    }

//    @org.junit.Test
    public void testChange() {
        System.out.println("Alterar");
        book = new Book(2, "Harry Potter e a Câmara Secreta", "Rocco", "J.K. Rowling", "6543-2", "2000", "Harry, no seu segundo...");
        daoImpl = new BookDaoImpl();
        daoImpl.change(book);
    }

//    @org.junit.Test
    public void testDelete() {
        System.out.println("Deletar");
        daoImpl = new BookDaoImpl();
        daoImpl.delete(1);
    }

//    @org.junit.Test
    public void testList() {
        System.out.println("Listar");
        daoImpl = new BookDaoImpl();
        List<Book> data = daoImpl.listAll();
        for (Book book : data) {
            System.out.println("ID: " + book.getId());
            System.out.println("Título: " + book.getTitle());
            System.out.println("Editora: " + book.getPublisher());
            System.out.println("Autor: " + book.getAuthor());
            System.out.println("Código: " + book.getCode());
            System.out.println("Ano: " + book.getYear());
            System.out.println("Descrição: " + book.getDescription());
        }
    }

//    @org.junit.Test
    public void testListById() {
        int id = 2;
        System.out.println("Listar por ID");
        daoImpl = new BookDaoImpl();
        Book book = daoImpl.listById(id);
        System.out.println("ID: " + book.getId());
        System.out.println("Título: " + book.getTitle());
        System.out.println("Editora: " + book.getPublisher());
        System.out.println("Autor: " + book.getAuthor());
        System.out.println("Código: " + book.getCode());
        System.out.println("Ano: " + book.getYear());
        System.out.println("Descrição: " + book.getDescription());
    }

//    @org.junit.Test
    public void testListByTitle() {
        String title = "Harry Potter";
        System.out.println("Listar por Nome");
        daoImpl = new BookDaoImpl();
        List<Book> data = daoImpl.listByTitle(title);
        for (Book book : data) {
            System.out.println("ID: " + book.getId());
            System.out.println("Título: " + book.getTitle());
            System.out.println("Editora: " + book.getPublisher());
            System.out.println("Autor: " + book.getAuthor());
            System.out.println("Código: " + book.getCode());
            System.out.println("Ano: " + book.getYear());
            System.out.println("Descrição: " + book.getDescription());
            System.out.println(" ");
        }
    }
}
