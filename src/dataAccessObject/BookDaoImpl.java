package dataAccessObject;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Book;

/**
 *
 * @author Tiago Martins Vargas
 *
 */
public class BookDaoImpl implements Serializable {

    private Connection connection;
    private PreparedStatement prepared;
    private ResultSet result;
    private Book book;

    public void save(Book book) {
        String query = "INSERT INTO book(title, publisher, author, code, year, description)"
                + "VALUES(?, ?, ?, ?, ?, ?)";

        try {
            connection = ConnectionFactory.getConnection();
            prepared = connection.prepareStatement(query);
            prepared.setString(1, book.getTitle());
            prepared.setString(2, book.getPublisher());
            prepared.setString(3, book.getAuthor());
            prepared.setString(4, book.getCode());
            prepared.setString(5, book.getYear());
            prepared.setString(6, book.getDescription());
            prepared.executeUpdate();
        } catch (Exception e) {
            System.out.println("Erro ao salvar o livro" + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(connection, prepared);
        }
    }

    public void change(Book book) {
        String query = "UPDATE book SET title = ?, publisher = ?, author = ?, code = ?, year = ?, description = ? WHERE idbook = ?";
        try {
            connection = ConnectionFactory.getConnection();
            prepared = connection.prepareStatement(query);
            prepared.setString(1, book.getTitle());
            prepared.setString(2, book.getPublisher());
            prepared.setString(3, book.getAuthor());
            prepared.setString(4, book.getCode());
            prepared.setString(5, book.getYear());
            prepared.setString(6, book.getDescription());
            prepared.setInt(7, book.getId());
            prepared.executeUpdate();
        } catch (Exception e) {
            System.out.println("Erro ao alterar o livro" + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(connection, prepared);
        }
    }

    public void delete(int id) {
        String query = "DELETE FROM book WHERE idbook = ?";
        try {
            connection = ConnectionFactory.getConnection();
            prepared = connection.prepareStatement(query);
            prepared.setInt(1, id);
            prepared.executeUpdate();
        } catch (Exception e) {
            System.out.println("Erro ao deletar livro" + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(connection, prepared);
        }
    }

    public List<Book> listAll() {
        String query = "SELECT * FROM book";
        List<Book> data = new ArrayList<>();
        try {
            connection = ConnectionFactory.getConnection();
            prepared = connection.prepareStatement(query);
            result = prepared.executeQuery();
            while (result.next()) {
                book = new Book();
                book.setId(result.getInt("idbook"));
                book.setTitle(result.getString("title"));
                book.setPublisher(result.getString("publisher"));
                book.setAuthor(result.getString("author"));
                book.setCode(result.getString("code"));
                book.setYear(result.getString("year"));
                book.setDescription(result.getString("description"));
                data.add(book);
            }
        } catch (Exception e) {
            System.out.println("Erro ao listar todos os correntista" + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(connection, prepared);
        }
        return data;
    }

    public Book listById(int id) {
        String query = "SELECT * FROM book WHERE idbook = ?";
        try {
            connection = ConnectionFactory.getConnection();
            prepared = connection.prepareStatement(query);
            prepared.setInt(1, id);
            result = prepared.executeQuery();
            book = new Book();
            result.next();
            book.setId(result.getInt("idbook"));
            book.setTitle(result.getString("title"));
            book.setPublisher(result.getString("publisher"));
            book.setAuthor(result.getString("author"));
            book.setCode(result.getString("code"));
            book.setYear(result.getString("year"));
            book.setDescription(result.getString("description"));
        } catch (Exception e) {
            System.out.println("Erro ao pesquisar livro por id" + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(connection, prepared);
        }
        return book;
    }

    public List<Book> listByTitle(String title) {
        String query = "SELECT * FROM book WHERE title LIKE ?";
        List<Book> data = new ArrayList<>();
        try {
            connection = ConnectionFactory.getConnection();
            prepared = connection.prepareStatement(query);
            prepared.setString(1, "%" + title + "%");
            result = prepared.executeQuery();
            while (result.next()) {
                book = new Book();
                book.setId(result.getInt("idbook"));
                book.setTitle(result.getString("title"));
                book.setPublisher(result.getString("publisher"));
                book.setAuthor(result.getString("author"));
                book.setCode(result.getString("code"));
                book.setYear(result.getString("year"));
                book.setDescription(result.getString("description"));
                data.add(book);
            }
        } catch (Exception e) {
            System.out.println("Erro ao listar os livros pelo título" + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(connection, prepared);
        }
        return data;
    }
}
