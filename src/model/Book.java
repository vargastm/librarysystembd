package model;

/**
 *
 * @author Tiago Martins Vargas
 *
 */
public class Book {

    private Integer id;
    private String title;
    private String publisher;
    private String author;
    private String code;
    private String year;
    private String description;

    public Book() {
    }

    public Book(Integer id, String title, String publisher, String author, String code, String year, String description) {
        this.id = id;
        this.title = title;
        this.publisher = publisher;
        this.author = author;
        this.code = code;
        this.year = year;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
